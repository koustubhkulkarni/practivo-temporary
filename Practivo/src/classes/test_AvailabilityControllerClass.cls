@istest
public with sharing class test_AvailabilityControllerClass {
     
     static testmethod void testAllFunctional1()
     {
     	AvailabilityController AvailabilityControllerObj = new AvailabilityController();
     	Contact SP = TestDataUtils.getServiceProvider();
     	test.startTest();
     	AvailabilityControllerObj.test1(SP);
      	test.stopTest();
     	
     }
}