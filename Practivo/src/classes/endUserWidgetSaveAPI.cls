@RestResource(urlMapping='/Appointed/endUserWidget/save') ///Appointed 
global class endUserWidgetSaveAPI {
     @HttpPost
     global static void saveAppointment()
     {
        /* SAVE APPOINTMENT FROM END WEBSITES To SALESFORCE
        @MethodName : saveAppointment
        @Author : Darshan Chhajed.
        @Description : This is API for Saving Appointment from End User Widget. 
                       In case of class appointment we are adding participant in a class. 
        */
        try
        {
            RestRequest httpRequest = RestContext.request;
            RestResponse httpResponse = RestContext.response;
            map<string,string> fetchDataMap =  new map<String,string>(); 
            string returnValue='Error on Server';
            system.debug('**endUserWidgetSaveAPI : RestContext.request.params**'+RestContext.request.params);
            for(string str :  RestContext.request.params.KeySet())
            {
                system.debug('putting map key '+str + 'value '+ RestContext.request.params.get(str));
                fetchDataMap.put(str, RestContext.request.params.get(str));
            }
            boolean isClassAppointment;
            if(fetchDatamap.containsKey('isClassApp'))
                isClassAppointment = Boolean.valueOf(fetchDataMap.get('isClassApp'));
            if(!isClassAppointment) //Save a new appointment
            {                           
                returnValue = ScheduleWizardController.validateAppointment(fetchDataMap);
            }
            else// add participant to class 
            {
                returnValue = ScheduleWizardController.addCustomerToAppointmentFromAPI(fetchDataMap);
            }     
            returnValue='{ "Message" :" ' + returnValue + '"}';     
            httpResponse.addHeader('Content-Type', 'application/json');
            httpResponse.addHeader('Access-Control-Allow-Origin', '*');
            httpResponse.addHeader('Access-Control-Allow-Methods', '*');  
            httpResponse.responseBody = Blob.valueOf(returnValue);
        }
        catch(Exception e)
        {
            system.debug('endUserWidgetSaveAPI--> Exception-->'+e.getStackTraceString()+'Messgae--'+e.getMessage());
        }    
     }
}