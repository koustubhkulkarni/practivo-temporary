public without sharing class DatabaseQueriesWithOutSharing {
    public DatabaseQueriesWithOutSharing()
    {}
    
    public static list<Event> getEventsWithoutSharing(string condition,Date startDate,Date endDate,list<string> locationCatIdList)
    {
        list<Event> lst;
        try
        {
            if(Utils.getReadAccessCheck('Event',new String []{'Id','Subject','Capacity__c', 'StartDateTime', 'EndDateTime', 'WhoId', 'WhatId', 'Appointment_Type__c',
                                  'Description__c','Event_Type__c','Location_Category_Mapping__c','No_of_Participants__c','IsAllDayEvent','Appointment_Type__r.Name','Appointment_Type__r.DefaultDuration__c','Location_Category_Mapping__r.Location__r.Name','Location_Category_Mapping__r.Location__r.ID','Location_Category_Mapping__r.Category__r.name','Location_Category_Mapping__r.Category__r.Id','Appointment_Type__r.Maximum_Participant__c'}))
                lst = (list<Event>)Database.query(condition);
            else
                throw new Utils.ParsingException('No Read access to Event or Fields in Event.');
        }
        catch(Exception e)
        {
           system.debug('DatabaseQueriesWithOutSharing-->getEventsWithoutSharing-->'+e.getStackTraceString()+e.getMessage()); 
        }        
        return lst;
    }
    
    public static list<Location_Category_Mapping__c> getLocationCatMappings(string locationId, String categoryId, String serviceProviderId)
    {
        list<Location_Category_Mapping__c> lst;
        try
        {
            if(Utils.getReadAccessCheck(Utils.PackageNamespace+'Location_Category_Mapping__c',new String []{'Id','Contact__c','Name','Location__c','Category__c','Category__r.Id','Category__r.Name','Contact__r.Name','Contact__r.Id','Location__r.Name','Location__r.Id','Contact__r.PractitionerColor__c','Contact__r.Resource_Type__r.Id'}))
                lst= [SELECT Id,Name,Category__r.Id,Category__r.Name,contact__r.RecordType.DeveloperName,Contact__r.Name,Contact__r.Id,Location__r.Name,Location__r.Id,Contact__r.PractitionerColor__c,Contact__r.Resource_Type__r.Id FROM Location_Category_Mapping__c WHERE Contact__r.Id=:serviceProviderId AND Location__r.Id = :locationId AND  Category__r.Id =:categoryId LIMIT 49999];
            else
                throw new Utils.ParsingException('No Read access to Location_Category_Mapping__c or Fields in Location_Category_Mapping__c.'); 
            if(lst.size()>0)
            return lst;
        }
        catch(Exception e)
        {
            system.debug('DatabaseQueriesWithOutSharing-->getLocationCatMappings-->'+e.getStackTraceString()+e.getMessage()); 
        }        
        return null;    
    }
}