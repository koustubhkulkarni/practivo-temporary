@isTest
public class test_InstallScript 
{   
static testmethod void testInstallScript() 
{
  PostInstallClass postinstall = new PostInstallClass();
    Test.testInstall(postinstall, null);
    Test.testInstall(postinstall, new Version(1,0), true);
    //LabelChanger__c LabelChangerSettingsLocation= LabelChanger__c.getInstance('Location');
    //System.assertEquals('Location', String.valueof(LabelChangerSettingsLocation.value__c));
  }
}