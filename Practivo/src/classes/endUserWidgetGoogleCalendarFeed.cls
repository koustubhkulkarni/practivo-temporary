@RestResource(urlMapping='/Appointed/endUserWidget/googleCalendarFeed')
global class endUserWidgetGoogleCalendarFeed 
{
    
    /*  
    	Author : Darshan Chhajed.
        Description : This method pushesh feed to Heroku server which will be pushed to respective google account.
        			  It will fetch only appointments which are edited/created between last call and cusrrent call. 	 
    */
    @HttpGet
    global static void getAppointmentsForGoogleFeed()
    {
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.addHeader('Access-Control-Allow-Origin', '*');
        RestContext.response.addHeader('Access-Control-Allow-Methods', '*');
        RestContext.response.responseBody = Blob.valueOf(Utils.getAppoinmentsForGoogleFeed());
    } 
    
    /*  Author : Darshan Chhajed.
        Description : This method is used for enduser widget. This method validate the entity is trying to 
        have google ical feed on Heroku with contact in Salesforce.
    */
    
    @HttpPost
    global static void validateContact()
    {
        try
        {
            string methodValidateContact = 'validContact';
            string methoUpdateContact = 'updateContact';
            RestRequest httpRequest = RestContext.request;
            RestResponse httpResponse = RestContext.response;
            map<string,string> fetchDataMap =  new map<String,string>(); 
            for(string str :  RestContext.request.params.KeySet())
            {
                fetchDataMap.put(str, RestContext.request.params.get(str));
            }
            string returnValue = 'Error:Method Not Executed';
            if(fetchDataMap.containsKey('method') && fetchDataMap.get('method')==methodValidateContact)
            {
            	if(fetchDataMap.get('serviceProviderId')!=null && fetchDataMap.get('serviceProviderId')!='')
            		returnValue = Utils.validateContact(fetchDataMap.get('serviceProviderId'));	
            }
            else if(fetchDataMap.containsKey('method') && fetchDataMap.get('method')==methoUpdateContact)
            {
            	if(fetchDataMap.get('serviceProviderId')!=null && fetchDataMap.get('serviceProviderId')!='')
            		returnValue = Utils.updateContactGoogleFeed(fetchDataMap.get('serviceProviderId'));
            }
            httpResponse.addHeader('Content-Type', 'application/json');
            httpResponse.addHeader('Access-Control-Allow-Origin', '*');
            httpResponse.addHeader('Access-Control-Allow-Methods', '*');
            httpResponse.responseBody = Blob.valueOf(JSON.serialize(returnValue));
        }
        catch(exception e)
        {
            System.debug('endUserWidgetGoogleCalendarFeed-->validateContact-->'+e.getstacktracestring());
        }    
    }
    
}