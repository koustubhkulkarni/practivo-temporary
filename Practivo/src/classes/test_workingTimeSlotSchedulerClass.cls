@isTest
public with sharing class test_workingTimeSlotSchedulerClass {
  public static String CRON_EXP = '0 30 * * * ?';
static testmethod void testFunctional()
{
      Contact SP = TestDataUtils.getServiceProvider();
      Event e = TestDataUtils.geteventworkingtimeslot(SP);
      Test.startTest();

      // Schedule the test job
      String jobId = System.schedule('ScheduleApexClassTest',
                        CRON_EXP, 
                        new workingTimeSlotScheduler());
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
        System.assertNotEquals(null, ct);
        System.assertEquals(0, ct.TimesTriggered);
       test.stopTest();
}  
    static testmethod void testFunctionalNegative()
{
      Test.startTest();
      String jobId = System.schedule('ScheduleApexClassTest',
                        CRON_EXP, 
                        new workingTimeSlotScheduler());
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
       System.assertNotEquals(null, ct);
        System.assertEquals(0, ct.TimesTriggered);
       test.stopTest();
}  
}