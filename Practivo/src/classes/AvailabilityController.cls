/*
***************************************
 class name-AvailabilityController
 Class Description-Helper Class for Saving or fetching availability of service provider.
 *************************************
*/
public with sharing class AvailabilityController 
{
public list<Event> availableTimeSlot = new list<Event>{};
public map<Integer,list<timeWrapper>> TimeSlots{get;set;}
public list<Event> eventsForInsertion {get;set;}
public list<Event> eventsForDeletion    {get;set;}
Contact DefaultServiceProvider;
private map<Id,Location_Category_Mapping__c> MapLocCatMaping;
public boolean isAdmin;
public static string schedulerCustomSettingName = 'isWorkingTimeSlotSchedulerStarted';
public list<Id> whoIds= new list<Id>();
public boolean isBulkInsertion{get;set;}
list<Location_Category_Mapping__c> locationCategoryLst = new list<Location_Category_Mapping__c>();
public AvailabilityController()
{
    eventsForInsertion = new list<Event>();
    eventsForDeletion =  new list<Event>();
    initTimeSlots();
   isBulkInsertion=false;
}
public AvailabilityController( boolean isAdmin,Contact defaultServiceProvider)
{
    initTimeSlots();
    this.DefaultServiceProvider = defaultServiceProvider;
    this.isAdmin = isAdmin;
    
}
public AvailabilityController( boolean isAdmin,Contact defaultServiceProvider,Map<Id,Location_Category_Mapping__c> Maplocmaping)
{
    initTimeSlots();
    eventsForInsertion = new list<Event>();
    eventsForDeletion =  new list<Event>();
    this.DefaultServiceProvider = defaultServiceProvider;
    this.MapLocCatMaping=Maplocmaping;
    this.isAdmin = isAdmin;
    
}

public void addDefaultractitioner()
{
    if(DefaultServiceProvider != null) //Sandesh
    whoIds.add(this.DefaultServiceProvider.Id);
}
public string saveAllTimeSlots() 
{
    string returnMsg='';
    try
    {
        if(Utils.getCreateAccessCheck('Event', new String []{'subject','whoid','OwnerId','RecurrenceStartDateTime','DurationInMinutes','RecurrenceEndDateOnly','RecurrenceDayOfWeekMask','RecurrenceInterval','RecurrenceType','WhatId','ActivityDate','IsRecurrence','StartDateTime','EndDateTime','Event_Type__c','Appointment_Type__c','Location_Category_Mapping__c','External_Id__c'}))
         {  
            Database.insert(this.eventsForInsertion,true);
            for(Location_Category_Mapping__c lcm : locationCategoryLst)
            {
                lcm.isWorkingTimeSlotInserted__c = true;
            }
            returnMsg = 'Available Time Slot Inserted Successfully';
            if(Utils.getUpdateAccessCheck(Utils.PackageNamespace+'Location_Category_Mapping__c',new String []{'isWorkingTimeSlotInserted__c'}))
                update locationCategoryLst;
            else 
                throw new Utils.ParsingException('No Update access to Location Category People  Mapping  or Fields in Location Category People.');  
            startScheduler();
          }
          else
          {
            throw new Utils.ParsingException('No Create access to Event or Fields in Event.');
          }
    }
    catch (Exception e){
        //sandesh Removed getStackTraceString
        returnMsg = 'Error=' + e.getMessage();
    }
    return returnMsg;
}
//Sandesh
Private void initTimeSlots()
{
    this.TimeSlots = new map<Integer,list<timeWrapper>>();
    for(Integer index=1;index<=7;index++)
    {
        if(!TimeSlots.containsKey(index))
        {
            list<timeWrapper> tempLst = new list<timeWrapper>();
            TimeSlots.put(index, tempLst);
        }
    }
}
public list<Contact> fetchAllServiceProvider() 
{
    string condition =' WHERE RecordType.developername = \'' +  Utils.contact_ServiceProviderRecordType +'\'';
    list<Contact> ServiceProviders = DatabaseQueries.getContactData(condition,1); 
    return ServiceProviders;
}
/*
Below method feches all available timeslot of service provider if isAdmin value is false, else
it fetches particular service provider's availability.
*/
public void  fetchAllAvailableTimeSlot(boolean isAdmin)
{
    Date todayDate = Date.today();
    Date startDate = todayDate.toStartofWeek();
    Date endDate = startDate.addDays(6); //fetching for entire Week
    string condition ='';
    
   
    if(isAdmin)
    {
        condition = 'WHERE Event_Type__c = \''+ Utils.EventType_workingTimeSlot + '\' AND IsRecurrence=false AND DAY_ONLY(EVENT.StartDateTime)>=:startDate AND  DAY_ONLY(EVENT.StartDateTime)<=:endDate ORDER BY EVENT.StartDateTime';  
        availableTimeSlot = DatabaseQueries.getEventData(condition,startDate,endDate);
    }
    else if(!isAdmin && MapLocCatMaping!=null && MapLocCatMaping.keySet().size()>0) 
    {
        Set<Id> SetLocMapIds=MapLocCatMaping.KeySet();
        if(Utils.getReadAccessCheck('Event',new String []{'Id','Subject','Capacity__c', 'StartDateTime', 'EndDateTime', 'WhoId', 'WhatId','Appointment_Type__c',
                        'Description__c','Event_Type__c', 'Location_Category_Mapping__c','No_of_Participants__c','Appointment_Type__r.Maximum_Participant__c'}))
           { 
            availableTimeSlot = [SELECT Id,Subject,Capacity__c, StartDateTime, EndDateTime, WhoId, WhatId, Appointment_Type__c,
                             Appointment_Type__r.Maximum_Participant__c , Description__c, Event_Type__c, Location_Category_Mapping__c,
                              No_of_Participants__c FROM EVENT WHERE Location_Category_Mapping__c IN:SetLocMapIds AND 
                              Event_Type__c = :Utils.EventType_workingTimeSlot AND WhoId =: this.DefaultServiceProvider.Id AND IsRecurrence=false AND DAY_ONLY(EVENT.StartDateTime)>=:startDate AND  DAY_ONLY(EVENT.StartDateTime)<=:endDate ORDER BY EVENT.StartDateTime ASC];
           }
         else
         {
            throw new Utils.ParsingException('No Read access to Event or Fields in Event.');
         }  
    }   
    system.debug('availableTimeSlot : Controller==>'+availableTimeSlot);
    if(!isAdmin) 
        processTimeSlot(defaultServiceProvider);
}
public void startScheduler()
{
    EpracticeSettings__c schedulerSetting= EpracticeSettings__c.getInstance(schedulerCustomSettingName);
    if(schedulerSetting==null)
    {
        schedulerSetting = new EpracticeSettings__c(Name = schedulerCustomSettingName, Value__c = 'TRUE');
        if(Utils.getCreateAccessCheck(Utils.PackageNamespace+'EpracticeSettings__c', new String []{'Name','value__c'}))
          {  
            insert schedulerSetting;
            callScheduler();
          }
         else
          {
            throw new Utils.ParsingException('No Create access to EpracticeSettings__c');
          } 
    }
    else if(schedulerSetting.value__c!='')
    {
        //if(schedulerSetting.value__c=='FALSE' || schedulerSetting.value__c=='false')
        //sandesh
        if((schedulerSetting.value__c).equalsIgnoreCase('false'))
        {
            callScheduler();
            schedulerSetting.value__c='TRUE';
            if(Utils.getCreateAccessCheck(Utils.PackageNamespace+'EpracticeSettings__c', new String []{'value__c'}) && Utils.getUpdateAccessCheck(Utils.PackageNamespace+'EpracticeSettings__c', new String []{'value__c'}))
                upsert schedulerSetting;
            else
          {
            throw new Utils.ParsingException('No Create access to EpracticeSettings__c');
          }     
        }
    }
}
void callScheduler()
{
    string jobName='UpdateWorkingTimeSlots' + system.now();
    string cronExpression = '0 0 22 ? * 6L'; //Class runs the last Friday of every month at 10 PM.
    workingTimeSlotScheduler jobObject= new workingTimeSlotScheduler();
    system.schedule(jobName,cronExpression,jobObject);
}
public void processTimeSlot(Contact selectedServiceProvider)
{
    this.initTimeSlots();
    this.DefaultServiceProvider = selectedServiceProvider;
    list<timeWrapper> timeWraperList = new list<timeWrapper>();
    if(availableTimeSlot!=null && !availableTimeSlot.isEmpty())
    {
        for(Event E: availableTimeSlot)
        {
            if(E.WhoId==selectedServiceProvider.Id)
            {
                Date DateObj = E.StartDateTime.date();
                Time TimeObj = E.StartDateTime.Time();  
                DateTime Dt1= DateTime.newInstance(DateObj,TimeObj);
                //sandesh
                //system.debug('--AvailabilityController : processTimeSlot : StartDateTime--'+Dt1);
                DateObj = E.EndDateTime.date();
                TimeObj = E.EndDateTime.Time();
                DateTime Dt2= DateTime.newInstance(DateObj,TimeObj);
                //system.debug('--AvailabilityController : processTimeSlot : EndDateTime--'+Dt2);
                //1-MONDAY to 7-SUNDAY  
                if(Dt1.format('u')=='1')// && E.EndDateTime.format('u')=='1')
                    populateTimeSlots(1,E);
                else if (Dt1.format('u')=='2')//(E.StartDateTime.format('u')=='2' && E.EndDateTime.format('u')=='2')
                    populateTimeSlots(2,E);
                else if(Dt1.format('u')=='3')   //(E.StartDateTime.format('u')=='3' && E.EndDateTime.format('u')=='3')
                    populateTimeSlots(3,E);
                else if(Dt1.format('u')=='4')   //(E.StartDateTime.format('u')=='4' && E.EndDateTime.format('u')=='4')
                    populateTimeSlots(4,E);         
                else if(Dt1.format('u')=='5')   //(E.StartDateTime.format('u')=='5' && E.EndDateTime.format('u')=='5')
                    populateTimeSlots(5,E);
                else if(Dt1.format('u')=='6')       //(E.StartDateTime.format('u')=='6' && E.EndDateTime.format('u')=='6')
                    populateTimeSlots(6,E);
                else if(Dt1.format('u')=='7')   //(E.StartDateTime.format('u')=='7' && E.EndDateTime.format('u')=='7')
                    populateTimeSlots(7,E);
                    
            }                       
        }
    }   
}
void populateTimeSlots(integer key,Event E)
{
    //sandesh
    //system.debug('-- AvailabilityController : populateTimeSlots : Key -- '+key + '-- EVENT--'+E);
    list<timeWrapper> timeWrapLst= new list<timeWrapper>();
    timeWrapper timeWrapObj=  new timeWrapper(E.StartDateTime.format('HH:mm'),E.EndDateTime.format('HH:mm'),E.Location_Category_Mapping__c);
    if(TimeSlots.containsKey(key))
    {
        timeWrapLst = TimeSlots.get(key);
        timeWrapLst.add(timeWrapObj);
        TimeSlots.put(key,timeWrapLst);
    }   
}


public list<event> saveServiceProviderWorkingTimeSlot()
{
    list<Event> eventToBeInserted = new list<Event>();
    for(integer weekDay : TimeSlots.KeySet())
    {
   
      if(TimeSlots.containsKey(weekDay))
      {
         list<timeWrapper> timeWrapLst= TimeSlots.get(weekDay);
        
         if(timeWrapLst!=null && !(timeWrapLst.isEmpty()))
          {
            timeWrapper timeWrapperObj = new timeWrapper();
            timeWrapper timeWrapperObj1 = new timeWrapper();
            timeWrapperObj = timeWrapLst.get(0);
            if(timeWrapperObj!=null && timeWrapperObj.startTime!='' && timeWrapperObj.endTime!='' &&timeWrapperObj.startTime!=null && timeWrapperObj.endTime!=null)
            {   
               
                eventToBeInserted.add(getSpecificFeilds(timeWrapperObj,weekDay));               
            }
            if(timeWrapLst.size()>=2)
                {
                    timeWrapperObj1 = timeWrapLst.get(1);
                    if(timeWrapperObj1!=null && timeWrapperObj1.startTime!='' && timeWrapperObj1.endTime!='' && timeWrapperObj1.startTime!='' && timeWrapperObj1.endTime!=null)
                    {   
                        eventToBeInserted.add(getSpecificFeilds(timeWrapperObj1,weekDay));
                    }
                }
          
          }
      }      
    }
    eventToBeInserted = populateCommonFeilds(eventToBeInserted);
    this.eventsForInsertion.addAll(eventToBeInserted);
    //sandesh
    //System.debug('eventsForInsertion-->'+eventsForInsertion);
    return eventToBeInserted;   
}
list<event> populateCommonFeilds(list<event> eventLst)
{
    for(Event e : eventLst)
    {
        e.IsRecurrence = true;
        e.OwnerId = UserInfo.getUserId();
        e.Subject = 'Working timeslot for '+DefaultServiceProvider.Name;
        e.WhoId = DefaultServiceProvider.Id;
        e.WhatId=DefaultServiceProvider.Resource_Type__r.Id;
        e.Event_Type__c= Utils.EventType_workingTimeSlot;
        e.RecurrenceInterval = 1;
        e.RecurrenceType = 'RecursWeekly';
    }
    return eventLst;
}
public static integer getDurationInMinunte(integer startTime,Integer endTime)
{
    integer startHour,endHour,startMin,endMin,difference;
    startHour = startTime/100;
    startMin = math.mod(startTime,100);
    endHour = endTime/100;
    endMin = math.mod(endTime,100);
    if(endMin==0)
    {
        endMin = 60;
        endHour = endHour-1;
    }
    difference = (((endHour-startHour)*60)+(endMin-startMin));
    return difference;  
}
event getSpecificFeilds(timeWrapper timeWrapperObj,integer weekDay)
{
    Integer startTime,hour,minutes,duaration; 
    Date StartDate = Date.Today();
    StartDate = StartDate.toStartofWeek();
    Date EndDate;
    EndDate = StartDate.addDays(364);
    if(isBulkInsertion!=null && isBulkInsertion==true)
         EndDate = StartDate.addDays(360);
    else
         EndDate = StartDate.addDays(30);
    Event freeSlot= new Event();

    startTime = Utils.getMilitaryTime(timeWrapperObj.startTime,false);
    duaration = getDurationInMinunte(startTime,Utils.getMilitaryTime(timeWrapperObj.endTime,false));    
    hour= startTime/100;
    minutes =  math.mod(startTime,100);
    Time tym = Time.newInstance(hour, minutes, 0, 0);
    //system.debug('--AvailabilityController : getSpecificFeilds : Saving time as --'+DateTime.newInstance(StartDate, tym));
    freeSlot.RecurrenceStartDateTime = DateTime.newInstance(StartDate, tym);
    freeSlot.DurationInMinutes = duaration;
    freeSlot.RecurrenceEndDateOnly = EndDate;
    freeSlot.RecurrenceDayOfWeekMask =  getMaskDay(weekDay);
    freeSlot.Location_Category_Mapping__c=timeWrapperObj.LocMapId;
    //system.debug('duaration--'+duaration); sandesh
    return freeSlot;
}
public static integer getMaskDay(integer weekDay)
{
    if(weekDay==1)
        return 2;
    else if(weekDay==2)
        return 4;
    else if(weekDay==3)
        return 8;
    else if(weekDay==4)
        return 16;
    else if(weekDay==5)
        return 32;
    else if(weekDay==6)
        return 64;
    else if(weekDay==7)
        return 1;   
    else return 0;  
}
/*
Below method Deletes all the working timeslots of a particular service provider if isservprovider parameter is true else
it deletes all the working timeslots present in the system.
*/
public void deleteAllFreeSlot(Id WhoID,Set<Id> SetLoccatmapids,boolean isservprovider)
{
    try
    {
        if(SetLoccatmapids!=null && SetLoccatmapids.size()>0)
        {
            if(Utils.getReadAccessCheck(Utils.PackageNamespace+'Location_Category_Mapping__c',new String []{'Id','Name','isWorkingTimeSlotInserted__c'}))
            {
                locationCategoryLst = [SELECT Id,isWorkingTimeSlotInserted__c FROM Location_Category_Mapping__c WHERE ID IN:SetLoccatmapids LIMIT 2000];
            }
            else 
                throw new Utils.ParsingException('No Read access to Location Category People Mapping');  
        }
        //Sandesh 
        //string condition;
        if(isservprovider)
        {
            if(Utils.getReadAccessCheck('Event',new String []{'Id','Subject','Capacity__c', 'StartDateTime', 'EndDateTime', 'WhoId', 'WhatId', 'Appointment_Type__c',
                           'Description__c', 'Event_Type__c', 'Location_Category_Mapping__c', 'No_of_Participants__c','Appointment_Type__r.Maximum_Participant__c'}))
                {
                    eventsForDeletion=  [SELECT Id,Subject,Capacity__c, StartDateTime, EndDateTime, WhoId, WhatId, Appointment_Type__c, Appointment_Type__r.Maximum_Participant__c ,
                                        Description__c, Event_Type__c, Location_Category_Mapping__c, No_of_Participants__c FROM Event
                                        WHERE Event_Type__c =:Utils.EventType_workingTimeSlot And WhoId=:String.escapeSingleQuotes(WhoID) AND IsRecurrence=TRUE AND Location_Category_Mapping__c IN:SetLoccatmapids];
                }
            else
                {
                    throw new Utils.ParsingException('No Read access to Event or Fields in Event.');
                }    
        }
        else
        {   
            if(Utils.getReadAccessCheck('Event',new String []{'Id','Subject','Capacity__c', 'StartDateTime', 'EndDateTime', 'WhoId', 'WhatId', 'Appointment_Type__c',
                             'Description__c','Event_Type__c','Location_Category_Mapping__c', 'No_of_Participants__c','Appointment_Type__r.Maximum_Participant__c'}))
             {
               eventsForDeletion= [SELECT Id,Subject,Capacity__c, StartDateTime, EndDateTime, WhoId, WhatId, Appointment_Type__c, Appointment_Type__r.Maximum_Participant__c ,
                                    Description__c, Event_Type__c, Location_Category_Mapping__c, No_of_Participants__c FROM Event
                                    WHERE IsRecurrence=TRUE];  //delete for all time slot
            }
            else
            {
                throw new Utils.ParsingException('No Read access to Event or Fields in Event.');
            }
        }
        if (Event.sObjectType.getDescribe().isDeletable())
        {
            delete eventsForDeletion;
            for(Location_Category_Mapping__c lcm : locationCategoryLst)
            {
                lcm.isWorkingTimeSlotInserted__c = false;
            }
        }
        else
            throw new Utils.ParsingException('No Delete access to Event.');
        
    }
    catch(Exception e)
    {
        System.debug('AvailabilityController-->deleteAllFreeSlot-->'+e.getMessage()+e.getStackTraceString());
    }
}
/*
Below method Processes input received from UI and adds it in timewrapper list for further operations.
*/
public void ProcessSaveWTs(Map<string,String> Savedata,Contact ServiceProvider,ID LocMapId)
{
    TimeSlots.clear();
    try{
        DefaultServiceProvider = ServiceProvider;
            list<timeWrapper> tempLst = new list<timeWrapper>();
            if(Savedata.get('WTSDay_1')=='false')
            {
                timeWrapper obj = new timeWrapper('','','');
                timeWrapper obj1 = new timeWrapper('','','');
                tempLst.add(obj);
                tempLst.add(obj1);
            }
            else
            {
                timeWrapper obj = new timeWrapper(saveData.get('start1_1'),saveData.get('end1_1'),LocMapId);
                timeWrapper obj1 = new timeWrapper(saveData.get('start2_1'),saveData.get('end2_1'),LocMapId);
                tempLst.add(obj);
                tempLst.add(obj1);
            }
            TimeSlots.put(1,tempLst);

            list<timeWrapper> tempLst1 = new list<timeWrapper>();
            if(Savedata.get('WTSDay_2')=='false')
            {
                timeWrapper obj2 = new timeWrapper('','','');
                timeWrapper obj3 = new timeWrapper('','','');
                tempLst1.add(obj2);
                tempLst1.add(obj3);
            }
            else
            {
                timeWrapper obj2 = new timeWrapper(saveData.get('start1_2'),saveData.get('end1_2'),LocMapId);
                timeWrapper obj3 = new timeWrapper(saveData.get('start2_2'),saveData.get('end2_2'),LocMapId);
                tempLst1.add(obj2);
                tempLst1.add(obj3);
            }
            TimeSlots.put(2,tempLst1);

            list<timeWrapper> tempLst2 = new list<timeWrapper>();
            if(Savedata.get('WTSDay_3')=='false')
            {
                timeWrapper obj4 = new timeWrapper('','','');
                timeWrapper obj5 = new timeWrapper('','','');
                tempLst2.add(obj4);
                tempLst2.add(obj5);
            }
            else
            {
                timeWrapper obj4 = new timeWrapper(saveData.get('start1_3'),saveData.get('end1_3'),LocMapId);
                timeWrapper obj5 = new timeWrapper(saveData.get('start2_3'),saveData.get('end2_3'),LocMapId);
                tempLst2.add(obj4);
                tempLst2.add(obj5);
            }
            TimeSlots.put(3,tempLst2);

            list<timeWrapper> tempLst3 = new list<timeWrapper>();
            if(Savedata.get('WTSDay_4')=='false')
            {
                timeWrapper obj6 = new timeWrapper('','','');
                timeWrapper obj7 = new timeWrapper('','','');
                tempLst3.add(obj6);
                tempLst3.add(obj7);
            }
            else
            {
                timeWrapper obj6 = new timeWrapper(saveData.get('start1_4'),saveData.get('end1_4'),LocMapId);
                timeWrapper obj7 = new timeWrapper(saveData.get('start2_4'),saveData.get('end2_4'),LocMapId);
                tempLst3.add(obj6);
                tempLst3.add(obj7);
            }
            TimeSlots.put(4,tempLst3);

            list<timeWrapper> tempLst4 = new list<timeWrapper>();
            if(Savedata.get('WTSDay_5')=='false')
            {
                timeWrapper obj8 = new timeWrapper('','','');
                timeWrapper obj9 = new timeWrapper('','','');
                tempLst4.add(obj8);
                tempLst4.add(obj9);
            }
            else
            {
                timeWrapper obj8 = new timeWrapper(saveData.get('start1_5'),saveData.get('end1_5'),LocMapId);
                timeWrapper obj9 = new timeWrapper(saveData.get('start2_5'),saveData.get('end2_5'),LocMapId);
                tempLst4.add(obj8);
                tempLst4.add(obj9);
            }
            TimeSlots.put(5,tempLst4);

            list<timeWrapper> tempLst5 = new list<timeWrapper>();
            if(Savedata.get('WTSDay_6')=='false')
            {
                timeWrapper obj10 = new timeWrapper('','','');
                timeWrapper obj11 = new timeWrapper('','','');
                tempLst5.add(obj10);
                tempLst5.add(obj11);
            }
            else
            {
                timeWrapper obj10 = new timeWrapper(saveData.get('start1_6'),saveData.get('end1_6'),LocMapId);
                timeWrapper obj11 = new timeWrapper(saveData.get('start2_6'),saveData.get('end2_6'),LocMapId);
                tempLst5.add(obj10);
                tempLst5.add(obj11);
            }
            TimeSlots.put(6,tempLst5);

            list<timeWrapper> tempLst6 = new list<timeWrapper>();
            if(Savedata.get('WTSDay_7')=='false')
            {
                timeWrapper obj12 = new timeWrapper('','','');
                timeWrapper obj13 = new timeWrapper('','','');
                tempLst6.add(obj12);
                tempLst6.add(obj13);
            }
            else
            {
               timeWrapper obj12 = new timeWrapper(saveData.get('start1_7'),saveData.get('end1_7'),LocMapId);
                timeWrapper obj13 = new timeWrapper(saveData.get('start2_7'),saveData.get('end2_7'),LocMapId);
                tempLst6.add(obj12);
                tempLst6.add(obj13);
            }
            TimeSlots.put(7,tempLst6);
    }
    catch(Exception e)
    {
        e.getMessage();
    }
    //saveTimeSlot();
}
public void test1(Contact C)
{
    //TimeSlots.clear();
    DefaultServiceProvider = c;
    fetchAllAvailableTimeSlot(false);
    //this.saveTimeSlot();
}
public void populateTimeSlotMap(Contact serviceProvider)
{
    this.TimeSlots.clear();
    DefaultServiceProvider = serviceProvider;
    processTimeSlot(serviceProvider);
}
public String getTimeslotJson()
{
    String TimeslotJson=JSON.serialize(TimeSlots);
    return TimeslotJson;
}
/*
Below method is asynchronous method which calls all necessary methods to insert availablity of the service provider.
*/
@future 
public static void insertWorkingTimeSlot(Id WhoID,Set<Id> SetLoccatmapids)
{
    list<event> availTimeSlots = new list<event>(); 
    Savepoint sp;
    try
    {
        Map<ID,Location_Category_Mapping__c> Maplocmaping;
        Contact serviceProvider;
        if(Utils.getReadAccessCheck(Utils.PackageNamespace+'Location_Category_Mapping__c',new String []{'Id','Name','Category__r.Id','Category__r.Name','Contact__r.Name','Contact__r.Salutation','Contact__r.Id','Location__r.Name','Location__r.Id','Contact__r.PractitionerColor__c','Contact__r.Resource_Type__r.Id'}))
           { 
                Maplocmaping= new Map<ID, Location_Category_Mapping__c>([SELECT Id,Name,Category__r.Id,Category__r.Name,contact__r.RecordType.DeveloperName,Contact__r.Name,Contact__r.Salutation,Contact__r.Id,Location__r.Name,Location__r.Id,Contact__r.PractitionerColor__c,Contact__r.Resource_Type__r.Id FROM Location_Category_Mapping__c WHERE ID IN:SetLoccatmapids]);
           }
        else
          {
            throw new Utils.ParsingException('No Read access to Location_Category_Mapping__c or Fields in Location_Category_Mapping__c.');
          }   
        if(Utils.getReadAccessCheck('Contact',new String []{'Id','Name','Email','firstname','lastname','Phone','Birthdate','RecordTypeId','PractitionerColor__c','Salutation','Start_Date_To_Generate_Calendar__c',
                                    'End_Date_To_Generate_Calendar__c','Resource_Type__r.Id'}))  
           {                           
            serviceProvider= [SELECT ID,Name,Email,firstname,lastname,Phone,Birthdate,RecordTypeId,PractitionerColor__c,Salutation,Start_Date_To_Generate_Calendar__c,
                                      End_Date_To_Generate_Calendar__c,Resource_Type__r.Id
                                      FROM Contact WHERE RecordType.developername =:String.escapeSingleQuotes(Utils.contact_ServiceProviderRecordType) AND ID=:String.escapeSingleQuotes(WhoID) LIMIT 1];
           }
        else
            {
              throw new Utils.ParsingException('No Read access to Contact or Fields in Contact.');  
            }    
            if(serviceProvider!=null && Maplocmaping!=null)
            {
                AvailabilityController availCntrOb = new AvailabilityController(false,serviceProvider,Maplocmaping);
                availCntrOb.fetchAllAvailableTimeSlot(false);
                availCntrOb.isBulkInsertion=true ;  //For All Service Peovider
                availCntrOb.saveServiceProviderWorkingTimeSlot();
                 sp = Database.setSavepoint();
                availCntrOb.deleteAllFreeSlot(WhoID,SetLoccatmapids,true);
                availCntrOb.saveAllTimeSlots();
            }      
    }catch(Exception e)
    {
        system.debug('==>   Exception : insertWorkingTimeSlot :'+e.getMessage()+'    Trace==>'+e.getStackTraceString());
        Database.rollback(sp);  
    }
}
public class timeWrapper{
    
    public string startTime {get;set;}
    public string endTime {get;set;}
    public String LocMapId{get;set;}
    public timeWrapper()
    {}
    timeWrapper(string startTym, string EndTym)
    {
        this.startTime = startTym;
        this.endTime = EndTym;
    }   
    timeWrapper(string startTym, string EndTym,String loccatmapid)
    {
        this.startTime=startTym;
        this.endTime=EndTym;
        this.LocMapId=loccatmapid;
    }   
}   
}