/*
***************************************
 class name-SendNotificationEmails
 Class Description-Class for sending emails to customers as well as Service Providers after creating new appointments in different scenarios.
 *************************************
*/
Public class SendNotificationEmails 
{
    public SendNotificationEmails(){}
    /*
     @MethodName : SendEmail
     @Return Type :  Void  
     @Author : Koustubh Kulkarni.
     @Description : This method is used to send emails to customers as well as Service Providers after creating new appointments in different scenarios.
   */
    public static boolean SendEmail(Set<id> SetEvIDs,Boolean isOnlyEvent,Boolean isUpdate)//Boolean variable should be passed true iff only event has been added/updated
  {
    list<Event> ListnewEvents;
    List<String> listEmailTemNames=new List<String>();
    List<EmailTemplate> lisEmailTemps=new List<EmailTemplate>();
    try
      {
        //Sandesh
        if(Utils.getReadAccessCheck('Event',new String []{'Id','IsRecurrence','No_of_Participants__c','Subject','Description__c','Location_Category_Mapping__c','Capacity__c','StartDateTime','EndDateTime','WhoId','Event_Type__c','WhatId','Appointment_Type__c','Appointment_Type__r.Maximum_Participant__c'}))  
            ListnewEvents=[SELECT Id ,Subject,Capacity__c, StartDateTime, 
                                  EndDateTime, WhoId, WhatId, Appointment_Type__c, Appointment_Type__r.Maximum_Participant__c , 
                                  Description__c, Event_Type__c, Location_Category_Mapping__c, No_of_Participants__c
                                  FROM event WHERE id IN:SetEvIDs LIMIT 2000];
         else
            throw new Utils.ParsingException('No Read access to Event or Fields in Event.');                         
         List<eventrelation> listevrels= new List<eventrelation>();
         list<Messaging.SingleEmailMessage> ListSingleMails = new list<Messaging.SingleEmailMessage>();
         EmailTemplate EmailTempService;
         EmailTemplate EmailTempCustomer;
         EmailTemplate EmailTempServiceCancelled;
         EmailTemplate EmailTempCustomerCancelled;
         EmailTemplate EmailTempServiceUpdated;
         EmailTemplate EmailTempCustomerUpdated;
         EmailTemplate EmailTempServiceEmptyClass;
         EmailTemplate EmailTempServiceProviderReminder;
         EmailTemplate EmailTempServiceProviderClsUpdate;
        
              //fetching all necessasry email templates
              listEmailTemNames.add('Email_Template_Pract_New_Appointment');
              listEmailTemNames.add('Email_Template_Cust_New_Appointment');
              listEmailTemNames.add('Email_Template_Pract_Cancelled_app');
              listEmailTemNames.add('Email_Template_Cust_Cancelled_app');
              listEmailTemNames.add('Email_Template_Cust_Update_Appointment');
              listEmailTemNames.add('Email_Template_Pract_Class_Appointment');
              listEmailTemNames.add('Email_Template_Pract_Updated_Appointment');
              listEmailTemNames.add('Email_Tem_Pract_UpdatedClass_Appointment');
              listEmailTemNames.add('Email_Template_Pract_Reminder');
              if(Utils.getReadAccessCheck('EmailTemplate',new String []{'Id'}))
                lisEmailTemps=[SELECT id,DeveloperName FROM EmailTemplate WHERE DeveloperName IN:listEmailTemNames];
              else
                throw new Utils.ParsingException('No Read access to EmailTemplate or Fields in EmailTemplate.');  
              if(lisEmailTemps!=null && !lisEmailTemps.isEmpty())
                for(EmailTemplate etemp:lisEmailTemps)
                {
                  if(etemp.DeveloperName=='Email_Template_Pract_New_Appointment')
                    EmailTempService=etemp;
                  else if(etemp.DeveloperName=='Email_Template_Cust_New_Appointment')  
                     EmailTempCustomer=etemp;
                  else if(etemp.DeveloperName=='Email_Template_Pract_Cancelled_app') 
                     EmailTempServiceCancelled=etemp; 
                  else if(etemp.DeveloperName=='Email_Template_Cust_Cancelled_app')
                      EmailTempCustomerCancelled=etemp;
                  else if(etemp.DeveloperName=='Email_Template_Cust_Update_Appointment')
                      EmailTempCustomerUpdated=etemp;
                  else if(etemp.DeveloperName=='Email_Template_Pract_Class_Appointment')
                      EmailTempServiceEmptyClass=etemp;
                  else if(etemp.DeveloperName=='Email_Template_Pract_Updated_Appointment')
                      EmailTempServiceUpdated=etemp;
                  else if(etemp.DeveloperName=='Email_Tem_Pract_UpdatedClass_Appointment')
                      EmailTempServiceProviderClsUpdate=etemp;
                  else if(etemp.DeveloperName=='Email_Template_Pract_Reminder')   
                      EmailTempServiceProviderReminder=etemp;
                }
             
            if(ListnewEvents!=null)
            {
               set<id> seteventids=(new Map<Id,SObject>(ListnewEvents)).keySet();
               set<id> setevwhoids=new set<id>();
               For (Event e1 : ListnewEvents)
               {
                  if(e1.whoid !=null)
                  {
                    setevwhoids.add(e1.whoid);
                  }
                  if(e1.WhatId !=null)
                  {
                    setevwhoids.add(e1.WhatId);
                  }
               }
               if(Utils.getReadAccessCheck('eventrelation',new String []{'Id','relationid','eventid'}))
                  listevrels=[SELECT id,eventid,relationid FROM eventrelation WHERE eventid IN:seteventids AND relationid NOT IN:setevwhoids LIMIT 2000];
                else
                  throw new Utils.ParsingException('No Read access to eventrelation or Fields in eventrelation.');
             }
             if(ListnewEvents!=null)
             {
             For (Event e1 : ListnewEvents)
                 {
                    if(e1.Event_Type__c==Utils.EventType_Appointment)//&& e1.Capacity__c!=0
                      {
                        if(!isUpdate)
                        {
                            Messaging.SingleEmailMessage MailSP=getmailmessage(e1.id,e1.whoid,EmailTempService.id,'New Appointment');
                            ListSingleMails.add(MailSP);
                            //Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});  
                            if(listevrels!=null)
                            {
                              For(Eventrelation evrel:listevrels)
                              {   
                                 Messaging.SingleEmailMessage MailCust = getmailmessage(evrel.eventid,evrel.relationid,EmailTempCustomer.id,'New Appointment');
                                 ListSingleMails.add(MailCust);
                                //Messaging.SendEmailResult [] r1 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail1});           
                              }  
                            }
                        }
                        else
                        {
                             Messaging.SingleEmailMessage MailSP=getmailmessage(e1.id,e1.whoid,EmailTempServiceUpdated.id,'Updated Appointment');
                            ListSingleMails.add(MailSP);
                            //Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});  
                            if(listevrels!=null)
                            {
                              For(Eventrelation evrel:listevrels)
                              {   
                                 Messaging.SingleEmailMessage MailCust = getmailmessage(evrel.eventid,evrel.relationid,EmailTempCustomerUpdated.id,'Updated Appointment');
                                 ListSingleMails.add(MailCust);
                                //Messaging.SendEmailResult [] r1 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail1});           
                              }  
                            }
                        }
                      }
                   else if(e1.Event_Type__c=='Cancelled')
                        {
                          Messaging.SingleEmailMessage MailSP=getmailmessage(e1.id,e1.whoid,EmailTempServiceCancelled.id,'Cancelled Appointment');
                          ListSingleMails.add(MailSP);
                          //Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});  
                          if(listevrels!=null)
                          {
                            For(Eventrelation evrel:listevrels)
                              {   
                                Messaging.SingleEmailMessage MailCust = getmailmessage(evrel.eventid,evrel.relationid,EmailTempCustomerCancelled.id,'Cancelled Appointment');
                                ListSingleMails.add(MailCust);
                               //Messaging.SendEmailResult [] r1 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail1});           
                              }
                           }     
                          } 
                    else if(e1.Event_Type__c==Utils.EventType_Partial)
                        {
                          if(!isUpdate)
                          {
                              Messaging.SingleEmailMessage MailSP=getmailmessage(e1.id,e1.whoid,EmailTempServiceEmptyClass.id,'Class Appointment');
                              if(isOnlyEvent==true)
                              {
                                ListSingleMails.add(MailSP);
                              }
                              //Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});  
                              if(listevrels!=null)
                              {
                                For(Eventrelation evrel:listevrels)
                                  {   
                                   Messaging.SingleEmailMessage MailCust=getmailmessage(evrel.eventid,evrel.relationid,EmailTempCustomer.id,'Class Appointment');
                                    ListSingleMails.add(MailCust);
                                   //Messaging.SendEmailResult [] r1 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail1});           
                                  }  
                              }
                          }
                          else
                          {
                              Messaging.SingleEmailMessage MailSP=getmailmessage(e1.id,e1.whoid,EmailTempServiceProviderClsUpdate.id,'Updated Class Appointment');
                              if(isOnlyEvent==true)
                              {
                                ListSingleMails.add(MailSP);
                              }
                              //Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});  
                              if(listevrels!=null)
                              {
                                For(Eventrelation evrel:listevrels)
                                  {   
                                   Messaging.SingleEmailMessage MailCust=getmailmessage(evrel.eventid,evrel.relationid,EmailTempCustomerUpdated.id,'Updated Appointment');
                                    ListSingleMails.add(MailCust);
                                   //Messaging.SendEmailResult [] r1 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail1});           
                                  }  
                              }
                          }
                        }  
                    if(!ListSingleMails.isEmpty())
                  {
                    Messaging.SendEmailResult [] mailresult = Messaging.sendEmail(ListSingleMails);
                      if(mailresult.get(0).isSuccess())
                          return true;
                      else
                          return false;
                    system.debug('SendEmailResult-->SendEmail-->'+mailresult.get(0).getErrors()[0].getMessage());
                  }
                  } 
              }  
      }
      catch(exception e)
      {
           System.debug('SendNotificationEmails-->SendEmail-->'+e.getMessage()+e.getStackTraceString());
          return false;
      }
      return false;
  }  
/*
     @MethodName : SendEmailtoSinglepatient
     @Return Type :  Void  
     @Author : Koustubh Kulkarni.
     @Description : This method is used to send emails to customers separately in case of class appointments(without sending mails to SP).
   */
public static boolean SendEmailtoSinglepatient(id Eventid,id relationid,Boolean iscancelled)
  {
    list<Messaging.SingleEmailMessage> ListSingleMails = new list<Messaging.SingleEmailMessage>();
    EmailTemplate EmailTempCustomer;
    EmailTemplate EmailTempCustomerCancelled;
    try
      {
       if(Utils.getReadAccessCheck('EmailTemplate',new String []{'Id'})) 
          EmailTempCustomer=[Select id from EmailTemplate where DeveloperName=:String.escapeSingleQuotes('Email_Template_Cust_New_Appointment')];
        else
           throw new Utils.ParsingException('No Read access to EmailTemplate or Fields in EmailTemplate.'); 
       if(Utils.getReadAccessCheck('EmailTemplate',new String []{'Id'}))
          EmailTempCustomerCancelled=[Select id from EmailTemplate where DeveloperName=:String.escapeSingleQuotes('Email_Template_Cust_Cancelled_app')];
        else
           throw new Utils.ParsingException('No Read access to EmailTemplate or Fields in EmailTemplate.'); 
       if(iscancelled==false)
       {
        Messaging.SingleEmailMessage MailCust = getmailmessage(Eventid,relationid,EmailTempCustomer.id,'Class Appointment');
        ListSingleMails.add(MailCust);
       }
       else
       {
        Messaging.SingleEmailMessage MailCust = getmailmessage(Eventid,relationid,EmailTempCustomerCancelled.id,'Cancelled Appointment');
        ListSingleMails.add(MailCust);
       }
       if(!ListSingleMails.isEmpty())
       {
        Messaging.SendEmailResult [] mailresult = Messaging.sendEmail(ListSingleMails);
           if(mailresult.get(0).isSuccess())
                    return true;
                else
                    return false;
       }
      }
    Catch(exception e)
      {
        System.debug('SendNotificationEmails-->SendEmailtoSinglepatient-->'+e.getMessage()+e.getstacktracestring());
                    return false;
      }
      return false;
  }
  /*
     @MethodName : sendEmailForExportCalendar
     @Return Type :  Void  
     @Author : Koustubh Kulkarni.
     @Description : This method is used to send email to Service Providers to send them ics file containing their appointments for particular duration.
   */
public static boolean sendEmailForExportCalendar(ID recipient, ID candidate,Boolean withtemplate,String message,String Stacktrace) 
{
    try
    {
      list<EmailTemplate> Listemailtemp;
      //New instance of a single email message
     Messaging.SingleEmailMessage mail = 
                new Messaging.SingleEmailMessage();
                List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
       if(withtemplate==true)
       {
         // Who you are sending the email to
          mail.setTargetObjectId(recipient);
          Id templateId;
          if(Utils.getReadAccessCheck('EmailTemplate',new String []{'Id','name'})) 
            Listemailtemp= [SELECT ID, name from EmailTemplate WHERE DeveloperName =:String.escapeSingleQuotes('Send_Appointment_Calendar')];
          else
            throw new Utils.ParsingException('No Read access to EmailTemplate or Fields in EmailTemplate.');  
         if(!Listemailtemp.isEmpty())
         {
          templateId=Listemailtemp[0].id;
         }
         // The email template ID used for the email
         mail.setTemplateId(templateId);
         mail.setWhatId(candidate);    
         mail.setBccSender(false);
         mail.setUseSignature(false);
         mail.setSenderDisplayName('Appointed Appointments');
         mail.setSaveAsActivity(false);  
      }
      else
      {
        //This else case is for sending email to admin after an exception in inserting working timeslots.
          mail.setTargetObjectId(recipient);
          mail.setSenderDisplayName('Appointed app exception');
          mail.setSubject('Appointed app exception:');
          mail.setBccSender(false);
          mail.setUseSignature(false);
          mail.setSaveAsActivity(false); 
          String plainTextBody = '';
          plainTextBody += 'Hi, \n';
          plainTextBody += 'There has been an error.Plese insert wokring time slots again. Sorry for the inconvinece. \n';
          plainTextBody += 'Here are the details regarding the error:- \n';
          plainTextBody += 'Error message- '+message+' \n';
          plainTextBody += 'Error Stacktrace- '+Stacktrace+' \n';
          plainTextBody += 'Thanks You. \n';
          mail.setPlainTextBody(plainTextBody);
      }
          Messaging.SendEmailResult [] mailresult=Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
                if(mailresult.get(0).isSuccess())
                    return true;
                else
                    return false;
    }
    Catch(exception e)
    {
        System.debug('Sendnotificationemails->sendEmailForExportCalendar-->'+e.getstacktracestring()+e.getmessage());
    }                
    return false;        
}
/*
Author : Darshan Chhajed
@Description : This method encrypts orgId and contactId and sends it to contacts for google calendar feed. 
?*/
public static String sendGoogleFeedKeyEmail(string serviceProviderId) 
{
    try
    {
        contact serviceProvder;
        if(Utils.getReadAccessCheck('Contact',new String []{'Id','Name'}))
        {
            list<contact> serviceProvderLst = [SELECT id,Name FROM Contact WHERE Id =:String.escapeSingleQuotes(serviceProviderId)];
            if(serviceProvderLst!=null&& serviceProvderLst.size()>0)
                serviceProvder = serviceProvderLst.get(0);
            else
                return 'Error : No Service Provider Found.';
            Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
            string encKeyDeveloperName = 'Encryption_Key';
            string encIVDeveloperName = 'Encryption_IV';
            string EndUserWidgetGoogleURL = 'EndUserWidgetGoogleURL';
          	  String URL = '';
          list<Appointed_Metadata__mdt> metaDataLst;
          if(Utils.getReadAccessCheck(Utils.PackageNamespace+'Appointed_Metadata__mdt',new String []{'DeveloperName','Value__c'}))
             metaDataLst = [SELECT DeveloperName,Value__c FROM Appointed_Metadata__mdt LIMIT 1000];
          else
            return 'Error : No Encryption Key Found';
          string orgId = UserInfo.getOrganizationId();
          string contactId = serviceProvder.Id; 
          Blob DataBlob = Blob.valueOf(orgId +'+'+ contactId);  
          system.debug(DataBlob.toString());
          Blob keyBlob, IVBlob;
          if(metaDataLst!=null && metaDataLst.size()>0)
          {
          	  for(Appointed_Metadata__mdt metadata : metaDataLst)
          	  {
          	  	if(metadata.DeveloperName == encKeyDeveloperName)
          	  		keyBlob =  EncodingUtil.base64Decode(metadata.value__c); 
          	  	else if(metadata.DeveloperName == encIVDeveloperName)
          	  		IVBlob = Blob.ValueOf(metadata.value__c);
          	  	else if(metadata.DeveloperName == EndUserWidgetGoogleURL)
          	  		URL = metadata.value__c; 
          	  }
          	  
          }
          else
            return 'Error : No Encryption Keys Found';
         //system.debug(EncodingUtil.base64Encode(keyBlob) + '  ' +IVBlob.toString()+  '  URL' + URL);
          Blob encrypted = Crypto.encrypt('AES192', Keyblob, IVBlob, DataBlob);
          //EpracticeSettings__c googleUrl = (EpracticeSettings__c.getInstance('endUserWidgetGoogleUrl'));
        //String.escapeSingleQuotes(googleUrl.value__c);
          mail.setTargetObjectId(serviceProviderId);
          mail.setSenderDisplayName('Appointed App');
          mail.setSubject('Your Appointed Google Key');
          mail.setBccSender(false);
          mail.setUseSignature(false);
          mail.setSaveAsActivity(false); 
          String plainTextBody = '';
          plainTextBody += 'Dear,'+ serviceProvder.Name +'\n';
          URL = URL + EncodingUtil.convertToHex(encrypted);
          plainTextBody += 'Please click on following link to register yourself with Appointed Google-feed Service,So that you sync Google Calendar with your Appointed Calendar \n';
          plainTextBody += 'Link - '+URL +' \n';
          plainTextBody += 'Thank You. \n';
          mail.setPlainTextBody(plainTextBody);
          Messaging.SendEmailResult [] mailresult=Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
            if(mailresult.get(0).isSuccess())
                return 'Success: Google-feed registration link has been sent to ' +serviceProvder.Name;
            else
                return 'Error: Failed to send mail.';
        }
        else
           return 'Error : You do not have read access to feilds on Contact';     
    }
    catch(exception e)
    {
        system.debug(e.getStackTraceString());
        return 'Error : Exception ' + e.getmessage();
    }                
    return 'Error:';        
}

public static boolean sendscheduledExportCalendar(List<Contact> LstSps)
{
  try
  {
    list<EmailTemplate> Listemailtemp;
    List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
    Id templateId;
    if(Utils.getReadAccessCheck('EmailTemplate',new String []{'Id','name'})) 
       Listemailtemp= [SELECT ID, name from EmailTemplate WHERE DeveloperName =:String.escapeSingleQuotes('Scheduled_Appointment_Calendar')];
    else
        throw new Utils.ParsingException('No Read access to EmailTemplate or Fields in EmailTemplate.');  
    if(!Listemailtemp.isEmpty())
    {
      templateId=Listemailtemp[0].id;
    }
    for(Contact con :LstSps) 
    {
        //New instance of a single email message
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setTargetObjectId(con.Id); 
      mail.setSaveAsActivity(false);
      mail.setTemplateId(templateId);
      mail.setWhatId(con.Id);    
      mail.setBccSender(false);
      mail.setUseSignature(false);
      mail.setSenderDisplayName('Appointed Appointments');
      allmsg.add(mail);
    }
    Messaging.SendEmailResult [] mailresult=Messaging.sendEmail(allmsg);
                if(mailresult.get(0).isSuccess())
                    return true;
                else
                    return false;
  }
  catch(Exception e)
  {
    System.debug('Sendnotificationemails-->sendscheduledExportCalendar-->'+e.getStackTraceString()+e.getMessage());
  }  
  return false;
}
  /*
     @MethodName : getmailmessage
     @Return Type :  Messaging.SingleEmailMessage  
     @Author : Koustubh Kulkarni.
     @Description : This method is used to generate Messaging.SingleEmailMessage object generically.
   */
public static Messaging.SingleEmailMessage getmailmessage(id evid,id evwhoid,id emailtemplateid,String dispname)
  {
      Messaging.SingleEmailMessage MailSP = new Messaging.SingleEmailMessage();
      try{
            MailSP.setTargetObjectId(evwhoid);
            MailSP.setWhatId(evid);     
            MailSP.setSenderDisplayName(dispname);
            MailSP.setUseSignature(false);
            MailSP.setBccSender(false);
            MailSP.setSaveAsActivity(false);  
            MailSP.setTemplateId(emailtemplateid);
            
          }
      catch(exception e)
      {
          System.debug('Sendnotificationemails-->getmailmessage-->'+e.getStackTraceString()+e.getMessage());
      }
    return MailSP;
  } 
}