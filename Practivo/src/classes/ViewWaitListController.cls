/*
***************************************
 class name-ViewWaitListController
 Class Description-Class for handling most of the waitlist related fetch operations.
 *************************************
*/
public with sharing class ViewWaitListController 
{
    public static list<event> listevent{get;set;}
    public static list<eventwrapper> listeventwrapper{get;set;}
    public static Map<ID, Contact> Mapcontacts{get;set;}
    public string evId{get;set;}   
    /*
     @MethodName : getFilteredeventwrapperlistjson
     @Return Type :  list<eventwrapper>  
     @Description : This method is used to fetch filtered waitlist based on the criterias of the provided events.
   */
    public Static list<eventwrapper> getFilteredeventwrapperlistjson(list<event> listofevents)
    {
        string jsonformattedlist;
        String str;
        try
        {
             listeventwrapper=new list<eventwrapper>();
             set<Id> setids=new set<Id>();
            if(listofevents!=null)
            {
                for(event ev:listofevents)
                {
                    for(eventrelation related:ev.eventrelations)
                    {
                        if(related.relationid!=ev.whatId)
                            setids.add(related.relationid);
                    }
                }
            }
            if(Utils.getReadAccessCheck('Contact',new String []{'Id','Name','PractitionerColor__c','Email','Phone','Salutation','RecordTypeId','Resource_Type__r.Id'}))
                Mapcontacts=new Map<ID, Contact>([SELECT id,name,Phone,Email,Salutation,RecordTypeId,PractitionerColor__c,Resource_Type__r.Id,RecordType.developername FROM contact WHERE (Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_CustomerRecordType) OR Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_ServiceProviderRecordType) OR Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_AssistantRecordType)) AND Id In:setids LIMIT 2000]);
            else
                throw new Utils.ParsingException('No Read access to Contact or Fields in Contact.');
            if(listofevents!=null)
            {
                for(event ev:listofevents)
                {
                    listeventwrapper.add(new eventwrapper(ev)); 
                }
            }
        }
        catch(exception ex)
        {
            System.debug('ViewWaitListController-->getFilteredeventwrapperlistjson--'+ex.getmessage()+ex.getstacktracestring());
        }
        return listeventwrapper;
    }
    /*
     @MethodName : getFilteredeventwrapperlistjson
     @Return Type :  String (Serialized Json) 
     @Description : This method is used to fetch global waitlist.
   */
    public Static String geteventwrapperlistjson()
    {
        string str;
        string str1;
        try
        {
           if(Utils.getReadAccessCheck('Event',new String []{'Id','createddate','WhoId','whatid', 'Appointment_Type__c','Description__c','Location_Category_Mapping__c','Appointment_Type__r.name','Location_Category_Mapping__r.Category__r.Name','Location_Category_Mapping__r.location__r.Name'}) && Utils.getReadAccessCheck('eventrelation',new String []{'Id','relationid','eventid'}))
                {
                    listevent=[SELECT id,whoid,createddate,Description__c,Appointment_Type__r.name,whatid,
                        Location_Category_Mapping__r.Category__r.Name,Location_Category_Mapping__r.location__r.Name,(SELECT id,relationid,eventid FROM eventrelations) 
                        FROM Event WHERE Event_Type__c=:String.escapeSingleQuotes(Utils.EventType_Waiting) and IsRecurrence=false LIMIT 2000];   
                }
            else
                throw new Utils.ParsingException('No Read access to Event or Fields in Event.');    
            listeventwrapper=new list<eventwrapper>();
            set<Id> setids=new set<Id>();
           if(listevent!=null)
            {
                for(event ev:listevent)
                {
                    for(eventrelation related:ev.eventrelations)
                    {
                        if(related.relationid!=ev.whatId)
                            setids.add(related.relationid);
                    }
                }
            }
            if(Utils.getReadAccessCheck('Contact',new String []{'Id','Name','PractitionerColor__c','Email','Phone','Salutation','RecordTypeId','Resource_Type__r.Id'}))
                Mapcontacts=new Map<ID, Contact>([SELECT id,name,Phone,Email,Salutation,RecordTypeId,PractitionerColor__c,Resource_Type__r.Id,RecordType.developername FROM contact WHERE (Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_CustomerRecordType) OR Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_ServiceProviderRecordType) OR Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_AssistantRecordType)) AND Id In:setids LIMIT 2000]);
            else
                throw new Utils.ParsingException('No Read access to Contact or Fields in Contact.');
            if(listevent!=null)
            {
                for(event ev:listevent)
                {
                    listeventwrapper.add(new eventwrapper(ev)); 
                }
            }
            str=JSON.serialize(listeventwrapper);
            str1=str.escapeHtml4();
            str1=String.escapeSingleQuotes(str1);
        }     
        catch(exception e)
        {
            system.debug('ViewWaitListController-->geteventwrapperlistjson--'+e.getmessage()+e.getstacktracestring());
        }      
     return str1;
    }
    public list<eventwrapper> geteventwrapperlist()
    {
        try
        {
            if(Utils.getReadAccessCheck('Event',new String []{'Id','createddate','WhoId','whatid', 'Appointment_Type__c','Description__c','Location_Category_Mapping__c','Appointment_Type__r.name','Location_Category_Mapping__r.Category__r.Name','Location_Category_Mapping__r.location__r.Name'}) && Utils.getReadAccessCheck('eventrelation',new String []{'Id','relationid','eventid'}))
                    listevent=[SELECT id,whoid,createddate,whatid,Description__c,Appointment_Type__r.name,Location_Category_Mapping__r.Category__r.Name,Location_Category_Mapping__r.location__r.Name FROM event WHERE Event_Type__c=:String.escapeSingleQuotes(Utils.EventType_Waiting) AND IsRecurrence=false LIMIT 2000];
            else
                 throw new Utils.ParsingException('No Read access to Event or Fields in Event.'); 
            listeventwrapper=new list<eventwrapper>(); 
            set<Id> setids=new set<Id>();
            if(listevent!=null)
            {
                for(event ev:listevent)
                {
                    for(eventrelation related:ev.eventrelations)
                    {
                        if(related.relationid!=ev.whatId)
                            setids.add(related.relationid);
                    }
                }
            }
            if(Utils.getReadAccessCheck('Contact',new String []{'Id','Name','PractitionerColor__c','Email','Phone','Salutation','RecordTypeId','Resource_Type__r.Id'}))
                Mapcontacts=new Map<ID, Contact>([SELECT id,name,Phone,Email,Salutation,RecordTypeId,PractitionerColor__c,Resource_Type__r.Id,RecordType.developername FROM contact WHERE (Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_CustomerRecordType) OR Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_ServiceProviderRecordType) OR Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_AssistantRecordType)) AND Id In:setids LIMIT 2000]);
            else
                throw new Utils.ParsingException('No Read access to Contact or Fields in Contact.');
            if(listevent!=null)
            {
                for(event ev:listevent)
                {
                    listeventwrapper.add(new eventwrapper(ev)); 
                }
            }
        }
        catch(exception e)
        {
            System.debug('ViewWaitListController-->geteventwrapperlist-->'+e.getmessage()+e.getstacktracestring());
        }
        
     return listeventwrapper;
    }
    
    public class eventwrapper
    {
        //This wrapper class contains all the fields which are necessary for displaying waitlist.
        public string eveid{get;set;}
        public string Name{get;set;}
        public string location{get;set;} 
        public string Category{get;set;} 
        public string Appointmenttype{get;set;}
        public string locationId{get;set;} 
        public string CategoryId{get;set;} 
        public string AppointmenttypeId{get;set;}
        public string ServiceproviderId{get;set;}
        public string addeddate{get;set;}  
        public string Des{get;set;}
        public string relid{get;set;}
        public string relname{get;set;}
        public string relconno{get;set;}
        public string relemail{get;set;}
        public eventwrapper(event ev)
        {
            Boolean checkForAnyCase;
            contact ConRecSP=getcontact(ev.whoid);
            string whoid=ev.whoid;
            this.eveid=ev.id;
            if(ConRecSP.RecordType.developername==Utils.contact_CustomerRecordType)
            {
                this.Name='Any';
                this.ServiceproviderId='';
                checkForAnyCase=true;
                this.relid=ConRecSP.Id;  
                this.relname=ConRecSP.Name;
                this.relconno=ConRecSP.Phone;
                this.relemail=ConRecSP.Email;
            }
            else if(ConRecSP.RecordType.developername==Utils.contact_ServiceProviderRecordType)
            {
                checkForAnyCase=false;
                this.Name=ConRecSP.Salutation+ConRecSP.Name;
                this.ServiceproviderId=ConRecSP.Id;
            }
            this.location=ev.Location_Category_Mapping__r.location__r.Name;
            this.locationId=ev.Location_Category_Mapping__r.location__r.Id;
            this.Category=ev.Location_Category_Mapping__r.Category__r.Name;
            this.CategoryId=ev.Location_Category_Mapping__r.Category__r.Id;
            this.AppointmenttypeId=ev.Appointment_Type__r.Id;
            this.Appointmenttype=ev.Appointment_Type__r.Name;
            this.addeddate=getdate(ev.createddate);
            this.Des=ev.Description__c;
            try
            {
                 for(EVENTrelation evr:ev.eventrelations)
                {
                    if(evr.eventid==ev.id && evr.relationid!=ev.whoid && checkForAnyCase==false)
                    {    
                        contact ConRecCust=getcontact(evr.relationid);
                        this.relid=evr.relationid;  
                        this.relname=ConRecCust.Name;
                        this.relconno=ConRecCust.Phone;
                        this.relemail=ConRecCust.Email;
                    }
                }
            }
            catch(exception e)
            {
               System.debug('ViewWaitListController-->eventwrapper-->'+e.getMessage()+e.getStackTraceString());
            }   
        }
        public string getdate(datetime cdate)
        {
            Date myDate;
            String StrmyDate;
            try
            {
                myDate = cdate.Date();
                StrmyDate=String.valueof(myDate);
            }
            catch(Exception e)
            {
                system.debug('ViewWaitListController-->getdate-->'+e.getMessage()+e.getStackTraceString());
            }    
             return StrmyDate;
        }
        public contact getcontact(id relid)
        {
            contact cont=new contact();
            try
            {
                System.debug('Mapcontacts'+Mapcontacts);
                if(Mapcontacts.containskey(relid))
                {
                    cont=Mapcontacts.get(relid);
                }
            }
            catch(exception ex)
            {
               system.debug('getcontact'+ex.getmessage()+ex.getstacktracestring());
            }
            return cont;
        }
    }

}