public with sharing class AddtoWaitListController
{
    public AddtoWaitListController()
    {    
        //listofpracs=[select id,name from contact where Recordtype.developername='Service_Provider'];
        
    }   
    public static void add(map<string,string> mapWaitlistdata)
    {
        //this method is used to add customer to waitlist
        //mapWaitlistdata contains all name value pairs coming from UI
        String patientemail;
         String ContactBirthdate;
         String Patientlastname;
         String Patientfirstname;
         String PatientPhone;
         String selectedPractitioner;
         String selectedLocation;
         String selectedCategory;
         String SelectedApptyp;
         String Note;
         String PractitionerName;
        try
        {
            Contact Patientinfo =new Contact();
            Recordtype rectype=new Recordtype();
            if(Utils.getReadAccessCheck('Recordtype',new String []{'Id'}))
                 {
                    rectype=[select id from Recordtype where Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_CustomerRecordType) and SobjectType='Contact' LIMIT 2000];
                 }
             else
                {
                    throw new Utils.ParsingException('No read access to Recordtype.');
                }    
            if(rectype!=null)
                Patientinfo.Recordtypeid=rectype.id;
            Event Eventdetails=new event();
            EventRelation evrel=new EventRelation();
            SYSTEM.DEBUG('SAMPLE Patientinfo---'+Patientinfo);
            if(mapWaitlistdata.containsKey('newappemail') && mapWaitlistdata.get('newappemail')!='')
            {
                patientemail=mapWaitlistdata.get('newappemail');  
            }
            else if(mapWaitlistdata.containsKey('extistingPatientEmail') && mapWaitlistdata.get('extistingPatientEmail')!='')
            {
                patientemail=mapWaitlistdata.get('extistingPatientEmail');
            }
            if(mapWaitlistdata.containsKey('newappBirthDate'))
            { 
                ContactBirthdate=mapWaitlistdata.get('newappBirthDate');
            }
            if(mapWaitlistdata.containsKey('newapplname'))
            { 
                 Patientlastname=mapWaitlistdata.get('newapplname');
            }
            if(mapWaitlistdata.containsKey('newappfname'))
            { 
                 Patientfirstname=mapWaitlistdata.get('newappfname');
            }
            if(mapWaitlistdata.containsKey('newappPhone'))
            { 
                 PatientPhone=mapWaitlistdata.get('newappPhone');
            }
            if(mapWaitlistdata.containsKey('selectPractitionerfornewapp'))
            {
                 selectedPractitioner=mapWaitlistdata.get('selectPractitionerfornewapp');
            }
            if(mapWaitlistdata.containsKey('selectLocationfornewapp'))
            { 
                 selectedLocation=mapWaitlistdata.get('selectLocationfornewapp');
            }
            if(mapWaitlistdata.containsKey('selectCategoryfornewapp'))
            { 
                 selectedCategory=mapWaitlistdata.get('selectCategoryfornewapp');
            }
            if(mapWaitlistdata.containsKey('selectatfornewapp'))
            { 
                  SelectedApptyp=mapWaitlistdata.get('selectatfornewapp');
            }
            if(mapWaitlistdata.containsKey('newappComments'))
            { 
                 Note=mapWaitlistdata.get('newappComments');
            }
            if(mapWaitlistdata.containsKey('selectPractitionerName'))
            {
                 PractitionerName=mapWaitlistdata.get('selectPractitionerName');
            }
            Date formatteddate;
            List<Location_Category_Mapping__c> loccatmap=new List<Location_Category_Mapping__c>(); 
            if(!String.isBlank(ContactBirthdate))
            {
                formatteddate = date.valueOf(ContactBirthdate);
            }
            string condition='WHERE RecordType.developername=\'Customer\' and email=\''+String.escapeSingleQuotes(patientemail)+'\'';
            list<contact> listpatients=DatabaseQueries.getContactData(condition,2);
                if(listpatients.isEmpty())
                    {  
                        if(!String.isBlank(ContactBirthdate))
                        {
                            Patientinfo.Birthdate=formatteddate;
                        }
                        Patientinfo.LastName=Patientlastname;
                        Patientinfo.FirstName=Patientfirstname;
                        Patientinfo.Email=patientemail;
                        Patientinfo.Phone=PatientPhone;
                        if(Utils.getCreateAccessCheck('Contact', new String []{'Birthdate','LastName','FirstName','Email','Phone','Recordtypeid'}))
                        {
                            insert Patientinfo;
                        }
                        else
                        {
                            throw new Utils.ParsingException('No Create access to Contact or Fields in contact.');
                        }
                    }
                else
                    {
                        Patientinfo=listpatients[0];
                    }
                    if(selectedPractitioner=='Any') //if practitioner preferance is Any
                    {
                        if(Utils.getReadAccessCheck(Utils.PackageNamespace+'Location_Category_Mapping__c',new String []{'Id'}))
                            loccatmap=[select id from Location_Category_Mapping__c where Location__c=:String.escapeSingleQuotes(selectedLocation) and Category__c=:String.escapeSingleQuotes(selectedCategory) LIMIT 1];
                        else
                            throw new Utils.ParsingException('No Read access to Location_Category_Mapping__c or Fields in Location_Category_Mapping__c.');
                    }
                    else
                    {
                        if(Utils.getReadAccessCheck(Utils.PackageNamespace+'Location_Category_Mapping__c',new String []{'Id'}))
                            loccatmap=[select id from Location_Category_Mapping__c where Location__c=:String.escapeSingleQuotes(selectedLocation) and Category__c=:String.escapeSingleQuotes(selectedCategory) and Contact__c=:String.escapeSingleQuotes(selectedPractitioner) LIMIT 2000];
                        else
                            throw new Utils.ParsingException('No Read access to Location_Category_Mapping__c or Fields in Location_Category_Mapping__c.');
                    }
                if(selectedPractitioner=='Any')
                {
                    SYSTEM.debug('inside any');
                Eventdetails.subject='Waiting List For Any';
                Eventdetails.Event_Type__c='Waiting';
                    Eventdetails.Appointment_Type__c=SelectedApptyp;
                    Eventdetails.whoid=null;
                    Eventdetails.IsRecurrence = false;
                    if(!loccatmap.isEmpty() && loccatmap!=null)
                    {
                        Eventdetails.Location_Category_Mapping__c=loccatmap[0].id;
                    }
                    Eventdetails.StartDateTime=datetime.now();//'1/31/2016 10:00 AM'
                    Eventdetails.EndDateTime=datetime.now();
                    Eventdetails.Description__c=Note;
                }
                else
                {
                    Eventdetails.subject='Waiting List For '+PractitionerName;
                    Eventdetails.Event_Type__c='Waiting';
                    Eventdetails.whoid=selectedPractitioner;
                    Eventdetails.Appointment_Type__c=SelectedApptyp;
                    Eventdetails.IsRecurrence = false;
                    if(!loccatmap.isEmpty())
                    {
                        Eventdetails.Location_Category_Mapping__c=loccatmap[0].id;
                    }
                    Eventdetails.StartDateTime=datetime.now();//'1/31/2016 10:00 AM'
                    Eventdetails.EndDateTime=datetime.now();
                    Eventdetails.Description__c=Note;
                }
           SYSTEM.debug('Eventdetails'+Eventdetails);
            if(Utils.getCreateAccessCheck('Event', new String []{'subject','whoid','IsRecurrence','StartDateTime','EndDateTime','Event_Type__c','Appointment_Type__c','Location_Category_Mapping__c','Description__c'}))
                {
                    insert Eventdetails;
                }
            else
                {
                    throw new Utils.ParsingException('No Create access to Event or Fields in Event.');
                } 
            evrel.EventId=Eventdetails.id;
            evrel.RelationId=Patientinfo.id;
            evrel.isparent=true;
            if(Utils.getCreateAccessCheck('EventRelation', new String []{'EventId','RelationId','isparent'}))
                insert evrel;
            else
                throw new Utils.ParsingException('No Create access to EventRelation or Fields in EventRelation.');
            //SYSTEM.debug('the event-'+Eventdetails);
        }
        catch(exception ex)
        {
            System.debug('AddtoWaitListController-->add-->'+ex.getmessage()+ex.getStackTraceString());
        }
    }
    public static void alsoaddtowait(ID EventId,Id CustomerID)
    {
        //this function is for also add me to waitlist functionality for end user widget
        Event Originalevent;
        Event WaitlistEvent=new event();
        EventRelation OriginaleventRelation;
        EventRelation WaitlistEventRelation=new EventRelation();
        try
        {
            if(Utils.getReadAccessCheck('Event',new String []{'Id','Subject','Capacity__c', 'StartDateTime', 'EndDateTime', 'WhoId', 'WhatId', 'Appointment_Type__c',
                          'Description__c','Event_Type__c','Location_Category_Mapping__c','No_of_Participants__c','Appointment_Type__r.Maximum_Participant__c'}))
                        { 
                            Originalevent=[SELECT Id,Subject,Capacity__c, StartDateTime, EndDateTime, WhoId, WhatId, Appointment_Type__c, Appointment_Type__r.Maximum_Participant__c ,
                                            Description__c, Event_Type__c, Location_Category_Mapping__c, No_of_Participants__c FROM Event WHERE ID=:String.escapeSingleQuotes(EventId) LIMIT 2000];
                        }
             else
                {           
                    throw new Utils.ParsingException('No Read access to Event or Fields in Event.');
                }
            if(Utils.getReadAccessCheck('eventrelation',new String []{'Id','relationid','eventid'}))              
               {
                    OriginaleventRelation=[SELECT id,relationid,eventid FROM eventrelation WHERE relationid=:String.escapeSingleQuotes(CustomerID) AND eventid=:String.escapeSingleQuotes(EventId) LIMIT 2000];
               } 
             else
                {
                    throw new Utils.ParsingException('No Read access to eventrelation or Fields in eventrelation.');
                }      
                    if(Originalevent!=null)
                    {
                        WaitlistEvent.subject='Waiting List';
                        WaitlistEvent.Event_Type__c='Waiting';
                        WaitlistEvent.whoid=Originalevent.whoid;
                        WaitlistEvent.Appointment_Type__c=Originalevent.Appointment_Type__c;
                        WaitlistEvent.IsRecurrence = false;
                        WaitlistEvent.Location_Category_Mapping__c=Originalevent.Location_Category_Mapping__c;
                        WaitlistEvent.StartDateTime=Originalevent.StartDateTime;
                        WaitlistEvent.EndDateTime=Originalevent.EndDateTime;
                        WaitlistEvent.Description__c=Originalevent.Description__c;
                    if(Utils.getCreateAccessCheck('Event', new String []{'subject','whoid','IsRecurrence','StartDateTime','EndDateTime','Event_Type__c','Appointment_Type__c','Location_Category_Mapping__c','Description__c'}))        
                        insert WaitlistEvent;
                    else
                        throw new Utils.ParsingException('No Create access to Event or Fields in Event.');    
                    }
                    if(OriginaleventRelation!=null)
                    {
                        WaitlistEventRelation.EventId=WaitlistEvent.id;
                        WaitlistEventRelation.RelationId=OriginaleventRelation.relationid;
                        WaitlistEventRelation.isparent=true;
                        if(Utils.getCreateAccessCheck('EventRelation', new String []{'EventId','RelationId','isparent'}))
                            insert WaitlistEventRelation;
                        else
                            throw new Utils.ParsingException('No Create access to EventRelation or Fields in EventRelation.');
                                
                    }
        }
        catch(exception ex)
        {
            System.debug('AddtoWaitListController-->alsoaddtowait-->'+ex.getmessage()+ex.getStackTraceString());
        }
    }
}