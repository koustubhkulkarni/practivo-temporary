trigger updateRecordTypeCount on Contact (before insert,before update) {

    list<sObject> contactLst = Trigger.New;
    try{
        updateRecordTypeCountHandler obj = new updateRecordTypeCountHandler();
        obj.updateRecordTypeCount(contactLst,'Contact');
    }catch(Exception e)
    {
        for(Contact c:Trigger.new)
            c.addError('Exception in Resource Count calculation'+e.getMessage());
    }
    
}